CREATE TABLE IF NOT EXISTS payments(
    id serial PRIMARY KEY,
    name text not null,
    value integer  not null,
    factor integer  not null
);