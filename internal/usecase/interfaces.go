// Package usecase implements application business logic. Each logic group in own file.
package usecase

import (
	"context"

	"payments/internal/entity"
)

type (
	Service interface {
		Create(ctx context.Context, user entity.User) (int, error)
		Get(ctx context.Context, id int) (*entity.User, error)
		Transfer(ctx context.Context, data entity.Transfer) error
		AddBalance(ctx context.Context, data entity.User) error
	}

	Repo interface {
		Create(context.Context, entity.User) (int, error)
		Get(ctx context.Context, id int) (*entity.User, error)
		Update(ctx context.Context, data entity.User) error
		ExecTx(ctx context.Context, fn func(ctx context.Context) error) error
	}
)
