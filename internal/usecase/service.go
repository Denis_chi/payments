package usecase

import (
	"context"
	"errors"
	"fmt"

	"payments/internal/entity"
)

// UseCase -.
type UseCase struct {
	repo Repo
}

func New(r Repo) *UseCase {
	return &UseCase{
		repo: r,
	}
}

func (uc *UseCase) Create(ctx context.Context, user entity.User) (int, error) {
	return uc.repo.Create(ctx, user)
}

func (uc *UseCase) Get(ctx context.Context, id int) (*entity.User, error) {
	user, err := uc.repo.Get(ctx, id)
	if err != nil {
		return nil, ErrUserNotFound
	}
	return user, nil
}

func (uc *UseCase) Transfer(ctx context.Context, data entity.Transfer) error {
	err := uc.repo.ExecTx(ctx, func(ctx context.Context) error {
		userFrom, err := uc.Get(ctx, data.From)
		if err != nil {
			return ErrUserNotFound
		}

		if userFrom == nil {
			return ErrUserNotFound

		}
		if userFrom.Balance.Value < data.Value {
			return fmt.Errorf("insufficient balance :%w", errors.New("insufficient funds"))
		}

		userTo, err := uc.Get(ctx, data.To)
		if err != nil {
			return ErrUserNotFound
		}
		if userTo == nil {
			return ErrUserNotFound

		}
		userFrom.Balance.Sub(data.Balance)
		userTo.Balance.Add(data.Balance)

		err = uc.repo.Update(ctx, *userFrom)
		if err != nil {
			return err
		}
		err = uc.repo.Update(ctx, *userTo)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func (uc *UseCase) AddBalance(ctx context.Context, userReq entity.User) error {
	user, err := uc.repo.Get(ctx, userReq.ID)
	if err != nil {
		return ErrUserNotFound
	}

	user.Balance.Add(userReq.Balance)
	return uc.repo.Update(ctx, *user)
}
