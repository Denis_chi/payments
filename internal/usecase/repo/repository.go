package repo

import (
	"context"
	"fmt"

	"payments/internal/entity"
	"payments/pkg/logger"
	"payments/pkg/postgres"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

// Repository -.
type Repository struct {
	pg *postgres.Postgres
	l  logger.Interface
}

// New -.
func New(pg *postgres.Postgres, l logger.Interface) *Repository {
	return &Repository{
		pg: pg,
		l:  l,
	}
}

type db interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (commandTag pgconn.CommandTag, err error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
}

func (r *Repository) conn(ctx context.Context) db {
	if tx, ok := ctx.Value(tansactionKey).(pgx.Tx); ok {
		return tx
	}
	return r.pg.Pool
}

func (r *Repository) ExecTx(ctx context.Context, fn func(ctx context.Context) error) error {
	if _, ok := ctx.Value(tansactionKey).(pgx.Tx); ok {
		return fn(ctx)
	}

	tx, err := r.pg.Pool.Begin(ctx)
	if err != nil {
		return err
	}

	ctx = context.WithValue(ctx, tansactionKey, tx)

	defer func() {
		if p := recover(); p != nil {
			if errRollback := tx.Rollback(ctx); errRollback != nil {
				r.l.Error("rollback err %s", errRollback)
			}
			err = fmt.Errorf("panic :%s", p)
			return
		}
		if errCommit := tx.Commit(ctx); errCommit != nil {
			r.l.Error("commit err %s", errCommit)
		}
	}()
	return fn(ctx)
}

func (r *Repository) Create(ctx context.Context, user entity.User) (int, error) {
	sql, args, err := r.pg.Builder.
		Insert(tablePayments).
		Columns("name", "value", "factor").
		Values(user.Name, 0, 100).
		Suffix("RETURNING id").
		ToSql()

	if err != nil {
		return 0, fmt.Errorf("Repository - r.Builder: %w", err)
	}
	var id int
	err = r.conn(ctx).QueryRow(ctx, sql, args...).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *Repository) Get(ctx context.Context, id int) (*entity.User, error) {
	sql, args, err := r.pg.Builder.
		Select("id", "name", "value", "factor").
		From(tablePayments).
		Where(squirrel.Eq{"id": id}).
		ToSql()

	if err != nil {
		return nil, fmt.Errorf("Repository - r.Builder: %w", err)
	}

	user := &entity.User{}
	err = r.conn(ctx).QueryRow(
		ctx,
		sql,
		args...,
	).Scan(
		&user.ID,
		&user.Name,
		&user.Balance.Value,
		&user.Balance.Factor,
	)
	if err != nil {
		return nil, fmt.Errorf("Repository - Get: %w", err)
	}

	return user, nil
}

func (r *Repository) Update(ctx context.Context, data entity.User) error {
	sql, args, err := r.pg.Builder.
		Update(tablePayments).
		Set("value", data.Balance.Value).
		Where(squirrel.Eq{"id": data.ID}).
		ToSql()
	if err != nil {
		return fmt.Errorf("Repository - r.Builder: %w", err)
	}

	_, err = r.conn(ctx).Exec(ctx, sql, args...)
	if err != nil {
		return fmt.Errorf("Repository - Update: %w", err)
	}

	return nil
}
