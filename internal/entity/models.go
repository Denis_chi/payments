package entity

type User struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Balance `json:"balance"`
}

type Balance struct {
	Value  int `json:"value"`
	Factor int `json:"factor"`
}

type Transfer struct {
	From int
	To   int
	Balance
}

func (b *Balance) Sub(balance Balance) {
	if b.Factor == balance.Factor {
		b.Value -= balance.Value
		return
	}
	factor := lcm(b.Factor, balance.Factor)
	b.Value = b.Value*factor/b.Factor - balance.Value*factor/balance.Factor
	b.Factor = factor
}

func (b *Balance) Add(balance Balance) {
	if b.Factor == balance.Factor {
		b.Value += balance.Value
		return
	}
	factor := lcm(b.Factor, balance.Factor)
	b.Value = b.Value*factor/b.Factor + balance.Value*factor/balance.Factor
	b.Factor = factor
}

func gcd(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func lcm(a, b int) int {
	return a * b / gcd(a, b)
}
