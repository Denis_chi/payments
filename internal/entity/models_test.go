package entity

// import (
// 	"testing"

// 	"github.com/stretchr/testify/assert"
// )

// func TestBalance_Sub(t *testing.T) {
// 	type args struct {
// 		balance Balance
// 	}
// 	tests := []struct {
// 		name string
// 		b    *Balance
// 		args args
// 		want Balance
// 	}{
// 		{
// 			name: "Subtracting balance with positive values",
// 			b:    &Balance{Value: 10, Factor: 2},
// 			args: args{balance: Balance{Value: 3, Factor: 2}},
// 			want: Balance{Value: 7, Factor: 2},
// 		},
// 		{
// 			name: "Subtracting balance with zero value",
// 			b:    &Balance{Value: 10, Factor: 2},
// 			args: args{balance: Balance{Value: 0, Factor: 1}},
// 			want: Balance{Value: 10, Factor: 2},
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			tt.b.Sub(tt.args.balance)
// 			assert.Equal(t, tt.want, *tt.b)
// 		})
// 	}
// }

// func TestBalance_Add(t *testing.T) {
// 	type args struct {
// 		balance Balance
// 	}
// 	tests := []struct {
// 		name string
// 		b    *Balance
// 		args args
// 		want Balance
// 	}{
// 		{
// 			name: "Adding balance with positive values",
// 			b:    &Balance{Value: 5, Factor: 3},
// 			args: args{balance: Balance{Value: 7, Factor: 3}},
// 			want: Balance{Value: 12, Factor: 3},
// 		},
// 		{
// 			name: "Adding balance with zero value",
// 			b:    &Balance{Value: 5, Factor: 3},
// 			args: args{balance: Balance{Value: 0, Factor: 1}},
// 			want: Balance{Value: 5, Factor: 3},
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			tt.b.Add(tt.args.balance)
// 			assert.Equal(t, tt.want, *tt.b)
// 		})
// 	}
// }

// func Test_gcd(t *testing.T) {
// 	type args struct {
// 		a int
// 		b int
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want int
// 	}{
// 		{
// 			name: "GCD of 20 and 30",
// 			args: args{a: 20, b: 30},
// 			want: 10,
// 		},
// 		{
// 			name: "GCD of 0 and 0",
// 			args: args{a: 0, b: 0},
// 			want: 0,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got := gcd(tt.args.a, tt.args.b)
// 			assert.Equal(t, tt.want, got)

// 		})
// 	}
// }

// func Test_lcm(t *testing.T) {
// 	type args struct {
// 		a int
// 		b int
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want int
// 	}{
// 		{
// 			name: "LCM of 2 and 5",
// 			args: args{a: 2, b: 5},
// 			want: 10,
// 		},
// 		{
// 			name: "LCM of 1 and 1",
// 			args: args{a: 1, b: 1},
// 			want: 1,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got := lcm(tt.args.a, tt.args.b)
// 			assert.Equal(t, tt.want, got)

// 		})
// 	}
// }
