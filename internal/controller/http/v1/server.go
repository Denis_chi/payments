package v1

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"payments/internal/entity"
	"payments/internal/usecase"
	"payments/pkg/logger"
)

type routes struct {
	s usecase.Service
	l logger.Interface
}

func newroutes(handler *gin.RouterGroup, t usecase.Service, l logger.Interface) {
	r := &routes{t, l}

	h := handler.Group("/payments")
	{
		h.POST("/create", r.Create)
		h.GET("/balance", r.Get)
		h.POST("/transfer", r.Transfer)
		h.POST("/add", r.AddBalance)
	}
}

func (r *routes) Create(c *gin.Context) {
	var user UserRequest
	err := c.BindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	id, err := r.s.Create(c.Request.Context(), entity.User{
		Name: user.Name,
	})
	if err != nil {
		r.l.Error(err, "http - v1 - create")
		errorResponse(c, http.StatusInternalServerError, "internal error")
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

func (r *routes) Get(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	idInt, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	user, err := r.s.Get(c.Request.Context(), idInt)
	if err != nil {
		errorResponse(c, http.StatusNotFound, err.Error())
		return
	}

	c.JSON(http.StatusOK, user)
}

func (r *routes) Transfer(c *gin.Context) {
	var transfer TransferRequest
	err := c.BindJSON(&transfer)
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	err = r.s.Transfer(c.Request.Context(), entity.Transfer{
		From: transfer.From,
		To:   transfer.To,
		Balance: entity.Balance{
			Value:  transfer.Value,
			Factor: 100,
		},
	})
	if err != nil {
		if errors.Is(err, usecase.ErrUserNotFound) {
			errorResponse(c, http.StatusNotFound, err.Error())
			return
		}

		r.l.Error(err, "http - v1 - transfer")
		errorResponse(c, http.StatusInternalServerError, "internal err")
		return
	}

	c.JSON(http.StatusOK, map[string]string{
		"status": "ok",
	})
}

func (r *routes) AddBalance(c *gin.Context) {
	var user UserRequest
	err := c.BindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	err = r.s.AddBalance(c.Request.Context(), entity.User{
		ID:   user.ID,
		Name: user.Name,
		Balance: entity.Balance{
			Value:  user.Value,
			Factor: user.Factor,
		},
	})
	if err != nil {
		if errors.Is(err, usecase.ErrUserNotFound) {
			errorResponse(c, http.StatusNotFound, err.Error())
			return
		}

		r.l.Error(err, "http - v1 - transfer")
		errorResponse(c, http.StatusInternalServerError, "internal err")
		return
	}

	c.JSON(http.StatusOK, map[string]string{
		"status": "ok",
	})
}
