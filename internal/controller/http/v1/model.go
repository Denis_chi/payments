package v1

type CreateRequest struct {
	Name string `json:"name"`
}

type UserRequest struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Balance `json:"balance"`
}

type Balance struct {
	Value  int `json:"value"`
	Factor int `json:"factor"`
}

type TransferRequest struct {
	From  int `json:"from"`
	To    int `json:"to"`
	Value int `json:"value"`
}
