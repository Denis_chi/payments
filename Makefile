LOCAL_BIN:=$(CURDIR)/bin
PATH:=$(LOCAL_BIN):$(PATH)

swag-v1: ### swag init
	swag init -g internal/controller/http/v1/router.go

run: swag-v1 ### swag run
	go mod tidy && go mod download && \
	DISABLE_SWAGGER_HTTP_HANDLER='' GIN_MODE=debug CGO_ENABLED=0 go run -tags migrate ./cmd/app


linter-golangci: ### check by golangci linter
	golangci-lint run

linter-hadolint: ### check by hadolint linter
	git ls-files --exclude='Dockerfile*' --ignored | xargs hadolint

linter-dotenv: ### check by dotenv linter
	dotenv-linter

test: ### run test
	go test -v -cover -race ./internal/...

mock: ### run mockgen
	mockgen -source ./internal/usecase/interfaces.go -package mock > ./internal/usecase/mock/mocks_interfaces.go
